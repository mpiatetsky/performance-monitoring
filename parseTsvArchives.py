import json
import csv
import os
import re
import numpy as np
from collections import defaultdict
from collections import Counter
import math

#parse each file
def parseFile(date,filename):
    pmatimes=[]
    pmaurltimes=defaultdict(list)
    pmtimes=[]
    pmurltimes=defaultdict(list)
    pmrequestcounter=Counter()
    pmarequestcounter=Counter()
    fullrequests=defaultdict(list)
    with open(filename) as f:
        for line in f:
            if 'nginx-access' in line:
                #pma case
                if ('prd-web-db-pma' in line) or ('prd-web-db-pma-2' in line):
                    handleCase(line.rstrip(),pmatimes,pmaurltimes,pmarequestcounter,fullrequests)
                #providermatch case
                elif ('prd-web-db-providermatch' in line) or ('prd-web-db-providermatch-3' in line):
                    handleCase(line.rstrip(),pmtimes,pmurltimes,pmrequestcounter,fullrequests)
    #pma case
    if pmatimes==[]:
        writeResults(date,0,{},{},{},{},{},'pmaResponseTimes.txt')
    else:
        #pmacase
        handleFinalCase(date,pmarequestcounter,pmatimes,pmaurltimes,fullrequests,'pmaResponseTimes.txt')
    #providermatch case
    handleFinalCase(date,pmrequestcounter,pmtimes,pmurltimes,fullrequests,'pmResponseTimes.txt')

def handleFinalCase(date,requestcounter,times,urltimes,fullrequests,outputfilename):
    meantime = round(np.mean(times),5)
    meanurltimes = getMeans(urltimes)
    percentiletimes95 = getPercentile(urltimes,95)
    percentiletimes99 = getPercentile(urltimes,99)
    writeResults(date,meantime,meanurltimes,percentiletimes95,percentiletimes99,requestcounter,fullrequests,outputfilename)

def getMeans(urltimes):
    meanurltimes=defaultdict(int)
    for key,value in urltimes.iteritems():
        meanurltimes[key]=round(np.mean(value),5)
    return meanurltimes

def getPercentile(urltimes,percentile):
    percentileurltimes=defaultdict(int)
    for key,value in urltimes.iteritems():
        percentileurltimes[key]=round(np.percentile(value,percentile),5)
    return percentileurltimes

def handleCase(line,times,urltimes,requestcounter,fullrequests):
    try:
        info=line.split('\t')[-1]
        [responseTime,request,fullrequest] = parseInfo(info,fullrequests)
        try:
            float(responseTime)
            if (responseTime != '0.000') and (responseTime != '-') and not math.isnan(float(responseTime)):
                times.append(float(responseTime))
                urltimes[request].append(float(responseTime))
                if request!=fullrequest:
                    urltimes[fullrequest].append(float(responseTime))
                    if fullrequest not in fullrequests[request]:
                        fullrequests[request].append(fullrequest)
                requestcounter[request]+=1
        except ValueError:
            pass
    except:
        pass

#parse the info we care about
def parseInfo(info,fullrequests):
    splitinfo=info.split(' ')
    responseTime=splitinfo[-1]
    try:
        fullrequest = re.search(r'"[\\x22GET|XGET|GET|POST|PUT|DELETE|HEAD](.*?)"',info).group()
    except:
        pass
    request=fullrequest[1:-10]
    fullrequest=fullrequest[1:-10]
    if '?' in request:
        if request[-1] is '?':
            request=request[:-1]
            fullrequest=fullrequest[:-1]
        else:
            request=request[0:request.index('?')]
    return [responseTime,request,fullrequest]

def writeResults(date,meantime,urlmeantimes,urlpercentiletimes95,urlpercentiletimes99,requestcounter,fullrequests,filename):
    with open(filename, 'a') as f:
        toWrite = date + '\t' + str(meantime) + '\t' + json.dumps(urlmeantimes)+ '\t' + json.dumps(urlpercentiletimes95)+'\t' + json.dumps(urlpercentiletimes99)+ '\t' + json.dumps(requestcounter)+ '\t' + json.dumps(fullrequests)+'\n'
        f.write(toWrite)

#get all files
files = os.listdir('.')
for f in files:
    if f[-4:]=='.tsv':
        print f
        parseFile(f[:-4],f)