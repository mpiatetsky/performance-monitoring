#Authors: Matthew Piatetsky and AJ Jenkins
import matplotlib.pyplot as plt
from collections import defaultdict
import numpy as np
import re
import json
from datetime import datetime

#returns average/95th percentile/99th percentile of daily response times and corresponding dates for nginx-access log lines for ProviderMatch and ProviderMatch Admin
def getAverageTimes(filename,request,flag,partial):
    dates=[]
    dateaverages=[]
    with open(filename,'r') as f:
        for line in f:
            #parse the relevant file
            [date,meantime,meanurltimes,percentileurltimes95,percentileurltimes99,requestcounter,parttofull] = line.split('\t')
            parttofull=json.loads(parttofull)
            #are we doing a graph of mean, 95th percentile or 99th percentile
            if flag is 'm':
                urltimes = meanurltimes
            elif flag is '95':
                urltimes = percentileurltimes95
            else:
                urltimes = percentileurltimes99
            #if request has been entered
            if request is not '':
                urltimes=json.loads(urltimes)
                if request in urltimes.keys():
                    #process a subquery search
                    if partial is not '':
                        temp=[]
                        #match the request + subquery to full urls
                        for fullurl in parttofull[request]:
                            if partial in fullurl:
                                requesttemp=fullurl
                                temp.append(urltimes[requesttemp])
                        if temp!=[]:
                            dates.append(date)
                            dateaverages.append(np.mean(temp))
                    else:
                        dateaverages.append(urltimes[request])
                        dates.append(date)
                else:
                    continue
            #note: the case with no request no longer works
            else:
                dateaverages.append(float(meantime))
    return [dates,dateaverages]

#plots daily average response times with corresponding dates as labels
def makeDayBarGraph(dates,dateaverages,request,flag,logscale,partial):
    fig = plt.figure(figsize=(16,10))
    plt.bar(range(len(dateaverages)),dateaverages,width=1)
    plt.xticks(range(len(dateaverages)),dates,rotation=270,ha='left')
    plt.xlabel("Date")
    plt.ylabel("Response Time - seconds")
    #change title for the correct case
    if request is not '':
        if partial is not '':
            if flag is 'm':
                plt.suptitle("Bar Graph of Mean Response Times over Time\nFor Request: " + request + "   With partial query: " + partial, y=0.98)
            else:
                plt.suptitle("Bar Graph of " + flag +"th Percentile Response Times over Time\nFor Request: " + request + "   With partial query: " + partial, y=0.98)
        else:
            if flag is 'm':
                plt.suptitle("Bar Graph of Mean Response Times over Time\nFor Request: " + request, y=0.98)
            else:
                plt.suptitle("Bar Graph of " + flag +"th Percentile Response Times over Time\nFor Request: " + request, y=0.98)
    else:
        if flag is 'm':
            plt.title("Bar Graph of Mean Response Times over Time")
        else:
            plt.title("Bar Graph of " + flag +"th Percentile Response Times over Time")
    if logscale:
        plt.yscale('log', nonposy='clip')
    plt.show()

#master function
def graphDailyAverageResponseTimes(filename,request,flag,logscale,partial):
    dates,dateaverages=getAverageTimes(filename,request,flag,partial)
    makeDayBarGraph(dates,dateaverages,request,flag,logscale,partial)

#process inputs
prodflag=raw_input("Write pm to graph ProviderMatch and write pma to graph ProviderMatch Admin. [pm|pma]\n")
filename=prodflag+'ResponseTimes.txt'
request=raw_input("Enter the request you would like to search.\n")
partial=raw_input("Would you like to enter a partial query parameter? Press enter otherwise.\n")
typeflag=raw_input("Write m for means graph, 95 for 95th percentile graph, or 99 for 99th percentile graph. [m|95|99]\n")
logscale=raw_input("Would you like a log scale? [y|n]\n")
logscale=True if logscale is 'y' else False

#run everything
graphDailyAverageResponseTimes(filename,request,typeflag,logscale,partial)